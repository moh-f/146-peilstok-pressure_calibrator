

#ifndef TSYS01_H
#define TSYS01_H

#include "mbed.h"
#include "debugger.h"

#define TSYS01_I2C_ADDR1 0xEE
#define TSYS01_I2C_ADDR0 0xEC

class TSYS01 {
public:
	TSYS01(I2C* _i2c, uint8_t adress);
	TSYS01(PinName SDA, PinName SCL, uint8_t adress);
	~TSYS01();

	void initialize();
	float readtemp();
	int16_t getInt16_t();


private:
	I2C *i2c;
	int _adress;
	uint8_t errorflag;

	uint16_t C[8];
	uint32_t D1;
	float TEMP;
	uint32_t adc;
	void calculate();
	uint32_t writeBytes(char *data, int length);
	uint32_t readBytes(char *data, int length);
};

#endif
