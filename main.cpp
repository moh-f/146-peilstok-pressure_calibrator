#include <mbed.h>
#include "nrf_soc.h"
#include "ble/BLE.h"
#include "ble/Gap.h"

#include "debugger.h"
#include "helpers.h"
#include "nrf_delay.h"
//#include "board_pins.h"
//#include "OWSlave.h"
//#include "custom_adc.h"
//#include "twtg_nrf_fstorage.h"

extern "C"
{
    //#include "softdevice_handler.h"
}

#include "debugger.h"
#include "math.h"
#include "TMP112.h"
#include "TSYS01.h"

#define PIN_LED_RED         P0_13
#define PIN_LED_GREEN       P0_14
#define PIN_LED_BLUE        P0_15

#define PIN_3V3SW_EN            P0_25
#define PIN_4VSW_EN             P0_26
#define PIN_TEMP_SDA           	P0_27
#define PIN_TEMP_SCL           	P0_28

#define PIN_HEATER        P0_12

// iBeacon
#define PAYLOAD_UUID_START        4
#define PAYLOAD_MAJOR_START       20
#define PAYLOAD_MINOR_START       22
#define PAYLOAD_TX_POWER_START    24

#define DEVICE_NAME                         "ECMobile"                                 /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME                   "TWTG"                                  /**< Manufacturer Name. Will be passed to Device Information Service. */
#define MODEL_NUMBER                        "Acacia ECM"                                 /**< Model Number. Will be passed to Device Information Service. */
#define SERIAL_NUMBER                       ""
#define HARDWARE_REVISION                   "Rev 1"                                     /**< Hardware revision. Will be passed to Device Information Service. */
#define FIRMWARE_REVISION                   "0.1.0"                                     /**< Firmware revision. Will be passed to Device Information Service. */
#define SOFTWARE_REVISION                   ""

static uint8_t iBeaconPayload[] = {
    0x4C, 0x00, // Company identifier code (0x004C == Apple)
    0x02,       // ID
    0x15,       // length of the remaining payload
    0xEF, 0x0D, 0xE7, 0xDA, 0x42, 0x6D, 0x4C, 0x86, // UUID
    0x8E, 0x78, 0xEF, 0x86, 0xBE, 0x6E, 0x9C, 0x83,
    0x00, 0x00, // the major value to differentiate a location
    0x00, 0x00, // the minor value to differentiate a location
    0xC8        // 2's complement of the Tx power (-56dB)
};

BLE ble;

I2C i2c(PIN_TEMP_SDA, PIN_TEMP_SCL);
TMP112 tmp112(&i2c, TMP112_I2C_ADDR);

// power
DigitalOut heater(PIN_HEATER, 0);
DigitalOut pwr_3v3sw_en(PIN_3V3SW_EN, 0);
DigitalOut pwr_4vsw_en(PIN_4VSW_EN, 0);

// led
DigitalOut led_red(PIN_LED_RED, 1);
DigitalOut led_green(PIN_LED_GREEN, 1);
DigitalOut led_blue(PIN_LED_BLUE, 1);


void bleInitComplete(BLE::InitializationCompleteCallbackContext *params)
{
    BLE &ble          = params->ble;
    ble_error_t error = params->error;

    if (error != BLE_ERROR_NONE) {
		//
        return;
    }

    // Ensure that it is the default instance of BLE
    if(ble.getInstanceID() != BLE::DEFAULT_INSTANCE) {
		
        return;
    }

    ble.gap().setDeviceName((const uint8_t *) DEVICE_NAME);
    ble.gap().setAppearance(GapAdvertisingData::GENERIC_TAG);
	
	ble.gap().clearAdvertisingPayload();
    ble.accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::MANUFACTURER_SPECIFIC_DATA, iBeaconPayload, sizeof(iBeaconPayload));

    ble.gap().clearScanResponse();
    ble.gap().accumulateScanResponse(GapAdvertisingData::COMPLETE_LOCAL_NAME, (uint8_t *) DEVICE_NAME, sizeof(DEVICE_NAME));
	
    // Set advertising type.
    ble.setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    ble.gap().setAdvertisingTimeout(0);
    ble.gap().setAdvertisingInterval(1000);
    ble.gap().startAdvertising();
} // */



void leds_set(bool red, bool green, bool blue)
{
	led_red = !red;
	led_green = !green;
	led_blue = !blue;
}

void leds_blink(bool red, bool green, bool blue, uint32_t count, uint32_t ms)
{
	for(uint8_t i = 0; i < count; i++)
	{
		leds_set(red, green, blue);
		wait_ms(ms);
		leds_set(0, 0, 0);
		wait_ms(ms);	
	}
}


int main(void)
{
	leds_blink(1, 1, 1, 3, 250);
	
	wait_ms(500);	
	heater = 0;
    pwr_3v3sw_en = 0;
    pwr_4vsw_en = 0;
    LOG("\r\n%s - %s\r\n", DEVICE_NAME, FIRMWARE_REVISION);
	
    BLE &ble = BLE::Instance();
    ble.init(bleInitComplete);
    while (!ble.hasInitialized());
	
	leds_blink(1, 0, 0, 3, 250);
    pwr_4vsw_en = 1;
		
	leds_blink(0, 1, 0, 3, 250);
    pwr_3v3sw_en = 1;
	//*/
	leds_blink(0, 0, 1, 3, 250);
	heater = 1;
	
	wait_ms(500);	
	leds_blink(0, 1, 0, 3, 250);
	    
	
    while (true) 
	{
		leds_set(0, 1, 0);
		wait_ms(50);
		leds_set(0, 0, 0);
		wait_ms(50);
		
		
		turnOffFloatInterrupt();
		ble.waitForEvent(); // allows or low power operation
    }
}

