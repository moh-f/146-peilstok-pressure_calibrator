
#include "TSYS01.h"
#include "mbed.h"

//todo fix this whole lib to somthing more nice

#define SYS01_REG_RESET 0x1E
#define SYS01_REG_START_CONV 0x48
#define SYS01_REG_RESULT 0x00
#define SYS01_REG_PROM_READ_0 0xA0
#define SYS01_REG_PROM_READ_1 0xA2
#define SYS01_REG_PROM_READ_2 0xA4
#define SYS01_REG_PROM_READ_3 0xA6
#define SYS01_REG_PROM_READ_4 0xA8
#define SYS01_REG_PROM_READ_5 0xAA
#define SYS01_REG_PROM_READ_6 0xAC
#define SYS01_REG_PROM_READ_7 0xAE

TSYS01::TSYS01(I2C* _i2c, uint8_t adress){
  i2c = _i2c;
  _adress = adress;
  errorflag = 0;
}

TSYS01::TSYS01(PinName SDA, PinName SCL, uint8_t adress){
  i2c = new I2C(SDA, SCL);
  _adress = adress;
  errorflag = 0;
}

TSYS01::~TSYS01(){

}


void TSYS01::initialize()
{
  char data[2];
  data[0] = SYS01_REG_RESET;
  writeBytes(data, 1);

  wait_ms(10);// Max boot time per datasheet
  
    // Read calibration values
  for ( uint8_t i = 0 ; i < 8 ; i++ ) {

    data[0] = SYS01_REG_PROM_READ_0 + i*2;
    writeBytes(data, 1);

    readBytes(data, 2);
    C[i] = (data[0] << 8) | data[1];
  }
}

float TSYS01::readtemp() {

  char data[3];
  data[0] = SYS01_REG_START_CONV;
  writeBytes(data, 1);

  wait_ms(10); // Max conversion time per datasheet
  
  data[0] = SYS01_REG_RESULT;
  writeBytes(data, 1);

  data[0] = 0;
  data[1] = 0;
  data[2] = 0;
  readBytes(data, 3);
  D1 = 0;
  D1 = data[0];
  D1 = (D1 << 8) | data[1];
  D1 = (D1 << 8) | data[2];

  calculate();
  return TEMP;
}

int16_t TSYS01::getInt16_t()
{
  return (int16_t)(readtemp() * 16.0f);
}

void TSYS01::calculate() {  
  if(D1 == 0){TEMP = 0;return;}
  adc = D1/256;

  TEMP = (-2) * float(C[1]) / 1000000000000000000000.0f * pow(adc,4) + 
        4 * float(C[2]) / 10000000000000000.0f * pow(adc,3) +
    (-2) * float(C[3]) / 100000000000.0f * pow(adc,2) +
        1 * float(C[4]) / 1000000.0f * adc +
      (-1.5) * float(C[5]) / 100 ;
}

uint32_t TSYS01::writeBytes(char *data, int length)
{
  return i2c->write( _adress, data, length);
}

uint32_t TSYS01::readBytes(char *data, int length)
{
  return i2c->read( _adress, data, length);
}

