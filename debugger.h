#ifndef CUSTOM_DEBUGGER_H
#define CUSTOM_DEBUGGER_H

//#define DEBUG_SEGGER

#define RTT_PRINT_DEFAULT
#define RTT_PRINT_ERROR
#define RTT_PRINT_WARNING


#ifdef DEBUG_SEGGER
#include "SEGGER_RTT2/SEGGER_RTT.h"

    #ifdef RTT_PRINT_DEFAULT
        #define RTTprintf(...) SEGGER_RTT_printf(0, __VA_ARGS__)
    #else
        #define RTTprintf(...) ;
    #endif

    #ifdef RTT_PRINT_ERROR
        #define RTT_ERROR(...) SEGGER_RTT_SetTerminal(1);SEGGER_RTT_printf(0, __VA_ARGS__);SEGGER_RTT_SetTerminal(0)
    #else
        #define RTT_ERROR(...) ;
    #endif

    #ifdef RTT_PRINT_WARNING
        #define RTT_WARNING(...) SEGGER_RTT_SetTerminal(2);SEGGER_RTT_printf(0, __VA_ARGS__);SEGGER_RTT_SetTerminal(0)
    #else
        #define RTT_WARNING(...) ;
    #endif
#else
    #define RTTprintf(...) ;
    #define RTT_ERROR(...) ;
    #define RTT_WARNING(...) ;
#endif

#define LOG RTTprintf

#endif//end of define CUSTOM_DEBUGGER_H
