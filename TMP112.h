

#ifndef TMP112_H
#define TMP112_H

#include "mbed.h"

#define TMP112_I2C_ADDR 0x92 //(four possible values)

class TMP112 {
public:
	TMP112(I2C* _i2c, uint8_t adress);
	TMP112(PinName SDA, PinName SCL, uint8_t adress);
	~TMP112();

	void changeAddress(uint8_t adress);

	void initialize();
	float readtemp();
	void setTempLow(int temp);
	void setTempHigh(int temp);

private:
	I2C *i2c;
	int _adress;
	uint8_t errorflag;

	uint32_t writeBytes(char *data, int length);
	uint32_t readBytes(char *data, int length);
};

#endif

