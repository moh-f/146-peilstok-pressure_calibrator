
#include "TMP112.h"
#include "mbed.h"

//todo fix this whole lib to somthing more nice

#define TMP102_CFG_default_byte1 0x61  // 12 bit WITH ShutDown bit turned ON as well
#define TMP102_CFG_default_byte2 0xA0  // just the defaults from pg 7
#define TMP102_OneShotBit 0x80  // one-shot by ORing D7 of CFG byte 1 to 1

// 1-byte pointer to write to tmp102 BEFORE reading back 2 bytes of data from that register
#define TMP102_TEMP_REG_pointer 0x00  // temperature register, read only, 16bits
#define TMP102_CONF_REG_pointer 0x01  // config register, read/write, 16 bits
#define TMP102_TLOW_REG_pointer 0x10  // temp Low  register, read/write, 16 bits
#define TMP102_THIGH_REG_pointer 0x11 // temp High register, read/write, 16 bits

TMP112::TMP112(I2C* _i2c, uint8_t adress){
  i2c = _i2c;
  _adress = adress;
  errorflag = 0;
}

TMP112::TMP112(PinName SDA, PinName SCL, uint8_t adress){
  i2c = new I2C(SDA, SCL);
  _adress = adress;
  errorflag = 0;
}

TMP112::~TMP112(){

}

void TMP112::changeAddress(uint8_t adress)
{
  _adress = adress;
}

void TMP112::initialize()
{

  //Sets to 12bit, sd mode on
  //none of these settings matter in one shot mode, at my temperature range, but set them to defaults anyway
  
    char data[3];
    data[0] = TMP102_CONF_REG_pointer;
    data[1] = TMP102_CFG_default_byte1;
    data[2] = TMP102_CFG_default_byte2;
    //i2c->write( _adress, data, 3);
    writeBytes(data,3);

    data[0] = TMP102_CONF_REG_pointer;
    data[1] = TMP102_CFG_default_byte1 | TMP102_OneShotBit;
    //data[2] = TMP102_CFG_default_byte2;
    //i2c->write( _adress, data, 2);
    writeBytes(data,2);
}



void TMP112::setTempLow(int temp) {
    char data[3];
    data[0] = TMP102_TLOW_REG_pointer;
    data[1] = data[0] >> 4;
    data[2] = data[1] << 4;
    i2c->write( _adress, data, 3);
}

void TMP112::setTempHigh(int temp) {
    char data[3];
    data[0] = TMP102_THIGH_REG_pointer;
    data[1] = data[0] >> 4;
    data[2] = data[1] << 4;
    i2c->write( _adress, data, 3);
}


float TMP112::readtemp() {
  float deg_c; 
  //errorflag=0; 
  // start by resetting the one shot bit back to zero
  char data[2];
    data[0] = TMP102_CONF_REG_pointer;
    data[1] = TMP102_CFG_default_byte1;
    writeBytes(data, 2);
  
    data[0] = TMP102_CONF_REG_pointer;
    data[1] = TMP102_CFG_default_byte1 | TMP102_OneShotBit;
    writeBytes(data, 2);

    wait_ms(1028);// use wdt to sleep here?
    
    data[0] = TMP102_TEMP_REG_pointer;
    writeBytes(data, 1);
    data[0] = 0;
    data[1] = 0;
    readBytes(data, 2);
    const uint8_t TempByte1 = data[0]; // MSByte, should be signed whole degrees C.
    const uint8_t TempByte2 = data[1]; // unsigned because I am not reading any negative temps
    const int Temp16 = (TempByte1 << 4) | (TempByte2 >> 4);    // builds 12-bit value
    
    //if(Temp16 & 0x800){Temp16 = temp16 ^ 0xFFF; Temp16 = -Temp16;}//negative numbers
    
    deg_c = (float)Temp16*(float) 0.0625;
    return deg_c;
}


uint32_t TMP112::writeBytes(char *data, int length)
{
  return i2c->write( _adress, data, length);
}

uint32_t TMP112::readBytes(char *data, int length)
{
  return i2c->read( _adress, data, length);
}