#include "mbed.h"
#include "debugger.h"

float map(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

uint16_t map(uint16_t x, uint16_t in_min, uint16_t in_max, uint16_t out_min, uint16_t out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void turnOffFloatInterrupt(void) //hot fix fpu interrupt keeps triggering
{
  __set_FPSCR(__get_FPSCR() & ~(0x0000009F));//magic number is from tha internet
  (void) __get_FPSCR();
  NVIC_ClearPendingIRQ(FPU_IRQn);
}


void printOnDevices()
{
    RTTprintf("NRF_SPI0->ENABLE\t0x%.2X\r\n", NRF_SPI0->ENABLE);
    RTTprintf("NRF_TWI0->ENABLE\t0x%.2X\r\n", NRF_TWI0->ENABLE);
    RTTprintf("NRF_UART0->ENABLE\t0x%.2X\r\n", NRF_UART0->ENABLE);
    RTTprintf("NRF_I2S->ENABLE\t0x%.2X\r\n", NRF_I2S->ENABLE);
    RTTprintf("NRF_SAADC->ENABLE\t0x%.2X\r\n", NRF_SAADC->ENABLE);
    RTTprintf("NRF_WDT->RUNSTATUS\t0x%.2X\r\n", NRF_WDT->RUNSTATUS);

    RTTprintf("NRF_PWM0->ENABLE\t0x%.2X\r\n", NRF_PWM0->ENABLE);
    RTTprintf("NRF_PWM1->ENABLE\t0x%.2X\r\n", NRF_PWM1->ENABLE);
    RTTprintf("NRF_PWM2->ENABLE\t0x%.2X\r\n", NRF_PWM2->ENABLE);
}

void printGPIO()
{
    uint32_t confn;

    for(int i=0; i<=31; i++)
    {
        confn = NRF_GPIO->PIN_CNF[i];

        RTTprintf("pin_conf[%u] = 0x%.4X", i, confn);
        wait_ms(100);

        if((confn & 0x1) == 1)
        {
            RTTprintf(" output,");
            wait_ms(100);
        }
        else
        {
            RTTprintf(" input,");
            wait_ms(100);
        }

        if(confn & 0x2)
        {
            RTTprintf(" Disconnect,");
            wait_ms(100);
        }

        if(confn & (0x4 | 0x8))
        {
            RTTprintf(" pullup,");
            wait_ms(100);
        }
        else if(confn & 0x4)
        {
            RTTprintf(" pulldown,");
            wait_ms(100);
        }

        RTTprintf("\r\n");
        wait_ms(100);
    }
}

void setPullPinMode(PinName pins, PinMode pMode)
{
  if(pMode == PullNone)
  {
    NRF_GPIO->PIN_CNF[pins] &= ~(GPIO_PIN_CNF_PULL_Msk);
  }
  else if(pMode == PullUp)
  {
    NRF_GPIO->PIN_CNF[pins] &= ~(GPIO_PIN_CNF_PULL_Msk);
    NRF_GPIO->PIN_CNF[pins] |= GPIO_PIN_CNF_PULL_Pullup << GPIO_PIN_CNF_PULL_Pos;
  }
  else if(pMode == PullDown)
  {
    NRF_GPIO->PIN_CNF[pins] &= ~(GPIO_PIN_CNF_PULL_Msk);
    NRF_GPIO->PIN_CNF[pins] |= GPIO_PIN_CNF_PULL_Pulldown << GPIO_PIN_CNF_PULL_Pos;
  }
}
